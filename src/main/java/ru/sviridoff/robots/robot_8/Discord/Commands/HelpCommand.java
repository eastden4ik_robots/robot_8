package ru.sviridoff.robots.robot_8.Discord.Commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import ru.sviridoff.robots.robot_8.Discord.CommandsHandler.Command;

public class HelpCommand extends Command {

    public HelpCommand(String name) {
        super(name);
    }

    @Override
    public void run(MessageReceivedEvent event) {
        System.out.println(super.getCommandName());
        event.getChannel().sendMessage(super.getCommandName()).queue();
    }

}
