package ru.sviridoff.robots.robot_8.Discord.Commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.robots.robot_8.Discord.CommandsHandler.Command;

public class UnknownCommand extends Command {

    private static Logger logger = new Logger();

    public UnknownCommand(String name) {
        super(name);
    }

    @Override
    public void run(MessageReceivedEvent event) {
        logger.Logging(Logger.LogLevel.WARNING, "Unknown Command: [" + event.getMessage().getContentDisplay().split(" ")[0] + "]", "log.txt");
        event.getChannel().sendMessage("Unknown Command: [" + event.getMessage().getContentDisplay().split(" ")[0] + "]").queue();
    }
}
