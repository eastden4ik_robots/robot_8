package ru.sviridoff.robots.robot_8.Discord.Commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;
import ru.sviridoff.robots.robot_8.Discord.CommandsHandler.Command;
import ru.sviridoff.robots.robot_8.Exceptions.AmountException;
import ru.sviridoff.robots.robot_8.Wargaming.Wargaming;

import java.io.IOException;

public class WotCommand extends Command {

    private static Logger logger = new Logger();
    private static Wargaming wargaming = new Wargaming();

    private static Props props;
    static {
        try {
            props = new Props("props.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public WotCommand(String s) {
        super(s);
    }

    @Override
    public void run(MessageReceivedEvent event) {
        logger.Logging(Logger.LogLevel.INFO, "Connecting to Wargaming ...", "log.txt");
        event.getChannel().sendMessage(super.getCommandName()).queue();
        String[] list = event.getMessage().getContentRaw().split(" ");

        try {
            if (list.length >= 3) {
                if (list[1].toLowerCase().equals("login")) {
                    int AccountId = wargaming.GetUserId(props, list[2]);
                    event.getChannel().sendMessage("У пользователя c логином [" + list[2] + "], ID = [" + AccountId + "].").queue();
                }
            } else {
                throw new AmountException("WRONG AMOUNT OF COMMANDS: [" + list.toString() + "].");
            }
        } catch (Exception ex) {
            logger.Logging(Logger.LogLevel.DANGER, "Error. " + ex.getMessage(), "log.txt");
        }

    }
}
