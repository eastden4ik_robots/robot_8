package ru.sviridoff.robots.robot_8.Discord.CommandsHandler;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

abstract public class Command {

    private String CommandName;

    public Command(String CommandName) {
        this.CommandName = CommandName;
    }

    public String getCommandName() {
        return CommandName;
    }

    public void run(MessageReceivedEvent event) {}


}
