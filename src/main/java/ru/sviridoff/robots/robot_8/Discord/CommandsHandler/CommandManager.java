package ru.sviridoff.robots.robot_8.Discord.CommandsHandler;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import ru.sviridoff.robots.robot_8.Discord.Commands.HelpCommand;
import ru.sviridoff.robots.robot_8.Discord.Commands.UnknownCommand;
import ru.sviridoff.robots.robot_8.Discord.Commands.WotCommand;


public class CommandManager {

    public void Manager(MessageReceivedEvent event) {

        if (event.getChannel().getName().equals("wot_info")) {
            String command = event.getMessage().getContentDisplay();

            switch (command.split(" ")[0].toLowerCase()) {
                case "help": {
                    new HelpCommand("Help Command.").run(event);
                    break;
                }
                case "wot": {
                    new WotCommand("WOT Command.").run(event);
                    break;
                }
                default: {
                    new UnknownCommand("Unknown Command.").run(event);
                    break;
                }
            }
        }


    }


    /*
    * Переделать мнеджер команд, как в вк боте. Там проще дорабатывать новые команды.
    * */

}
