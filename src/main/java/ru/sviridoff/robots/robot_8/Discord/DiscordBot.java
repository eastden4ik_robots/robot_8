package ru.sviridoff.robots.robot_8.Discord;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.robots.robot_8.Discord.CommandsHandler.CommandManager;

public class DiscordBot extends ListenerAdapter {

    private static Logger logger = new Logger();

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        if (!event.getAuthor().isBot()) {
            new CommandManager().Manager(event);
        } else {
            logger.Logging(Logger.LogLevel.WARNING, "Ботов мы не слушаем ...", "log.txt");
        }
    }

}
