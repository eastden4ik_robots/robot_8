package ru.sviridoff.robots.robot_8.Exceptions;

public class AmountException extends Exception {

    public AmountException(String name) {
        super(name);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public String getLocalizedMessage() {
        return super.getLocalizedMessage();
    }
}
