package ru.sviridoff.robots.robot_8.Exceptions;

public class NoAccountException extends Exception {

    public NoAccountException(String name) {
        super(name);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
