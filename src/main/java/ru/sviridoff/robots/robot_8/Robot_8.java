package ru.sviridoff.robots.robot_8;


import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDABuilder;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;
import ru.sviridoff.robots.robot_8.Discord.DiscordBot;

import java.io.IOException;

public class Robot_8 {


    private static Logger logger = new Logger();
    private static Props props;
    static {
        try {
            props = new Props("props.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception {
        logger.Logging(Logger.LogLevel.INFO, "Robot 8 (Wargaming info bot) is running ...", "log.txt");
        JDABuilder jda = new JDABuilder(AccountType.BOT);
        jda.setToken(props.getProperty("bot.discord.token"));
        jda.addEventListener(new DiscordBot());
        jda.buildAsync();
    }


}
