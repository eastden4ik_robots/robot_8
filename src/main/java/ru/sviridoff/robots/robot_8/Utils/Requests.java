package ru.sviridoff.robots.robot_8.Utils;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import ru.sviridoff.framework.logger.Logger;

public class Requests {

    private final CloseableHttpClient httpClient = HttpClients.createDefault();
    private static Logger logger = new Logger();

    public String sendGet(String URL) throws Exception {
        String result = "";
        HttpGet request = new HttpGet(URL);

        request.addHeader(HttpHeaders.USER_AGENT, "Discord_Bot");

        try (CloseableHttpResponse response = httpClient.execute(request)) {

            logger.Logging(Logger.LogLevel.INFO, response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            Header headers = entity.getContentType();
            logger.Logging(Logger.LogLevel.INFO, headers.toString());

            if (entity != null) {
                result = EntityUtils.toString(entity);
            }

        }
        return result;

    }

}
