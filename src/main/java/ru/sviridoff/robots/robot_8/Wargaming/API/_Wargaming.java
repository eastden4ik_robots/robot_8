package ru.sviridoff.robots.robot_8.Wargaming.API;

public class _Wargaming {


    public static final String USER_ID = "https://api.worldoftanks.ru/wot/account/list/?application_id=%s&search=%s";
    public static final String USER_INFO = "https://api.worldoftanks.ru/wot/account/info/?application_id=%s&account_id=%s&language=%s";
    public static final String FIELDS = "&fields=%2C%2C%2C%2C";

    // FIELDS

    public static final String FIELD_CLIENT_LANGUAGE = "client_language";
    public static final String FIELD_CREATION_DATE = "created_at";
    public static final String FIELD_GLOBAL_RATING = "global_rating";
    public static final String FIELD_LAST_BATTLE = "last_battle_time";
    public static final String FIELD_LOGOUT_TIME = "logout_at";

    // CONNECTOR OF THE FIELDS - %2C
    public static final String _C = "%2C";

}
