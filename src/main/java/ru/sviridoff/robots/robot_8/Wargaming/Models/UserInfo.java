package ru.sviridoff.robots.robot_8.Wargaming.Models;

public class UserInfo {


    private String client_language;
    private int created_at;
    private int last_battle_time;
    private int global_rating;
    private int logout_at;

    public String getClient_language() {
        return client_language;
    }

    public void setClient_language(String client_language) {
        this.client_language = client_language;
    }

    public int getCreated_at() {
        return created_at;
    }

    public void setCreated_at(int created_at) {
        this.created_at = created_at;
    }

    public int getLast_battle_time() {
        return last_battle_time;
    }

    public void setLast_battle_time(int last_battle_time) {
        this.last_battle_time = last_battle_time;
    }

    public int getGlobal_rating() {
        return global_rating;
    }

    public void setGlobal_rating(int global_rating) {
        this.global_rating = global_rating;
    }

    public int getLogout_at() {
        return logout_at;
    }

    public void setLogout_at(int logout_at) {
        this.logout_at = logout_at;
    }
}
