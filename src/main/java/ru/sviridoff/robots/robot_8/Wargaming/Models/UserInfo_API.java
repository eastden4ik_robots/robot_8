package ru.sviridoff.robots.robot_8.Wargaming.Models;

import java.util.Map;

public class UserInfo_API {

    private String status;
    private Meta meta;
    private Map<String, UserInfo> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Map<String, UserInfo> getData() {
        return data;
    }

    public void setData(Map<String, UserInfo> data) {
        this.data = data;
    }
}
