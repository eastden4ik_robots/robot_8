package ru.sviridoff.robots.robot_8.Wargaming;

import com.google.gson.Gson;

import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;
import ru.sviridoff.robots.robot_8.Exceptions.NoAccountException;
import ru.sviridoff.robots.robot_8.Utils.Requests;
import ru.sviridoff.robots.robot_8.Wargaming.API._Wargaming;
import ru.sviridoff.robots.robot_8.Wargaming.Models.UserId_API;

public class Wargaming {

    private static Requests requests = new Requests();
    private static Gson gson = new Gson();
    private static Logger logger = new Logger();

    public int GetUserId(Props props, String login) throws Exception {
        int result;

        String req = requests.sendGet(String.format(_Wargaming.USER_ID, props.getProperty("bot.wargaming.id"),login));
        logger.Logging(Logger.LogLevel.INFO, "Getting account id from nickname: ");
        UserId_API userId_api = gson.fromJson(req, UserId_API.class);
        if (userId_api.getStatus().equals("ok")) {
            logger.Logging(Logger.LogLevel.INFO, "Status: " + userId_api.getStatus());
            logger.Logging(Logger.LogLevel.INFO, "Count: " + userId_api.getMeta().getCount());
            logger.Logging(Logger.LogLevel.INFO, "Nickname: " + userId_api.getData().get(0).getNickname());
            logger.Logging(Logger.LogLevel.INFO, "Account ID: " + userId_api.getData().get(0).getAccount_id());
            result = userId_api.getData().get(0).getAccount_id();
        } else {
            logger.Logging(Logger.LogLevel.DANGER, "Error: " +
                    userId_api.getError().getMessage() + " - " +
                    userId_api.getError().getCode() + " , " +
                    "in field: " + userId_api.getError().getField() +
                    "with value: " + userId_api.getError().getValue());
            throw new NoAccountException("Error: " +
                    userId_api.getError().getMessage() + " - " +
                    userId_api.getError().getCode() + " , " +
                    "in field: " + userId_api.getError().getField() +
                    "with value: " + userId_api.getError().getValue());
        }

        return result;
    }


}
